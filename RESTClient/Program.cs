﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RESTClient
{
    class Program
    {
        static void Menu()
        {
            Console.Clear();
            Console.WriteLine("[1] - Pobierz macierz");
            Console.WriteLine("[2] - Wyślij macierz");
            Console.WriteLine("========================");
            Console.WriteLine("[0] - Exit");
        }
        static void Main(string[] args)
        {
            var _client = new RESTClient.MatrixClient(new System.Net.Http.HttpClient());
            bool exit = false;

            while(!exit)
            {
                Menu();
                var wybor = Console.ReadLine();

                switch(wybor)
                {
                    case "1":
                        var indexColl = _client.GetAvailableIndexes();

                        Console.Write($"(Dostepne indeksy: ");
                        foreach (var value in indexColl)
                            Console.Write($"{ value} ");
                        Console.WriteLine(")");
                        Console.Write("Podaj indeks: ");
                        if (!Int32.TryParse(Console.ReadLine(), out var index) && indexColl.Contains(index))
                        {
                            Console.WriteLine("Niepoprawny index");
                            Console.ReadKey();
                            break;
                        }
                        
                        var _matrix = new Matrix(_client.Get(index));
                        _matrix.Print();
                        Console.ReadKey();                                                                          
                        break;
                    case "2":
                        Console.WriteLine("Podaj wiersze: ");
                        if (!Int32.TryParse(Console.ReadLine(), out var rows))
                        {
                            Console.WriteLine("Niepoprawna wartosc!");
                            Console.ReadKey();
                            break;
                        }

                        Console.WriteLine("Podaj kolumny: ");
                        if (!Int32.TryParse(Console.ReadLine(), out var columns))
                        {
                            Console.WriteLine("Niepoprawna wartosc!");
                            Console.ReadKey();
                            break;
                        }

                        var matrix = new Matrix(rows,columns);
                        if (_client.Insert(matrix)<0)
                        {
                            Console.WriteLine("Usluga zwrocila blad!");
                            Console.ReadKey();
                        }
                        break;
                    case "0":
                        exit = true;
                        break;

                    default:
                        Console.WriteLine("Wybrales zla wartosc");
                        Console.ReadKey();
                        break;
                }
            }                                 
        }
    }
}
