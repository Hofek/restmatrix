﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;

namespace RESTClient
{
    public partial class Matrix
    {
        private int[,] _data;

        [JsonIgnore]
        public int Rows { get; set; }
        [JsonIgnore]
        public int Columns { get; set; }
        public Matrix(Response res)
        {
            if (res == null)
                return;

            var enumerator = res.Data.GetEnumerator(); 
            enumerator.MoveNext(); //set enumerator on first element

            _data = new int[res.Data.Count, enumerator.Current.Count];
            Rows = res.Data.Count;
            Columns = enumerator.Current.Count;

            int i = 0;
            int j = 0;
            foreach (var row in res.Data)
            {
                j = 0;
                foreach(var value in row)
                {
                    _data[i, j] = value;
                    ++j;
                }
                ++i;
            }                       
        }

        public Matrix(int rows, int columns)
        {
            Random rnd = new Random();
            Rows = rows;
            Columns = columns;
            _data = new int[rows, columns];


            Data = new List<ICollection<int>>();

            for (int i=0; i<rows; ++i)
            {
                var row = new List<int>();
                for (int j = 0; j < columns; ++j)
                {
                    _data[i, j] = rnd.Next(1, 100);
                    row.Add(_data[i, j]);
                }
                Data.Add(row); 
            }
        }

        public void Print()
        {
            for (int a=0; a<Rows; ++a)
            {
                for (int b = 0; b < Columns; ++b)
                {
                    Console.Write($"{_data[a, b]} ");
                }
                Console.WriteLine();
            }
                
        }
    }           
}
