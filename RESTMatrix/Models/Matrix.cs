﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace RESTMatrix.Models
{  
    public class Matrix
    {        
        public int[,] Data { get; set; }
        public Matrix(int rows, int columns)
        {
            Data = new int[rows, columns];
        }
    }
}