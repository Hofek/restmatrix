﻿using RESTMatrix.Models;
using Swashbuckle.Swagger.Annotations;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Web.Http;

namespace RESTMatrix.Controllers
{
    /// <summary>
    /// Obsługa operacji na macierzach
    /// </summary>
    [Route("matrix/")]
    public class MatrixController : ApiController
    {
        private static List<Matrix> _matrixes = new List<Matrix>() { new Matrix(2,3) };

        /// <summary>
        /// Zwrócenie macierzy z bazy danych
        /// </summary>
        /// <param name="id">ID macierzy</param>
        /// <returns>macierz</returns>
        [HttpGet, Route("Get", Name = "Pobierz Macierz")]              
        [SwaggerResponse(System.Net.HttpStatusCode.OK, type:typeof(Matrix))]
        [SwaggerResponse(System.Net.HttpStatusCode.NotFound)]         
        [AllowAnonymous]
        public HttpResponseMessage Get(int id)
        {
            if (id < _matrixes.Count && id > -1)
                return Request.CreateResponse<Matrix>(System.Net.HttpStatusCode.OK, _matrixes[id]);
            
            return Request.CreateResponse<Matrix>(System.Net.HttpStatusCode.NotFound, null);
        }

        /// <summary>
        /// Wysłanie macierzy na serwer
        /// </summary>
        /// <param name="matrix">Wysyłana macierz</param>        
        [AllowAnonymous]
        [HttpPost, Route("Upload", Name = "Wyślij Macierz")]
        [SwaggerResponse(System.Net.HttpStatusCode.OK, type:typeof(int))]
        [SwaggerResponse(System.Net.HttpStatusCode.InternalServerError, type: typeof(int))]
        public HttpResponseMessage Insert([FromBody]Matrix matrix)
        {
            if (matrix == null)
                return Request.CreateResponse<int>(System.Net.HttpStatusCode.InternalServerError, -1);

            _matrixes.Add(matrix);
            return Request.CreateResponse<int>(System.Net.HttpStatusCode.OK, _matrixes.Count-1);
        }

        /// <summary>
        /// Zwrócenie indeksów możliwych do zwrócenia macierzy
        /// </summary>
        /// <returns>Tablica indeksów</returns>
        /// [AllowAnonymous]
        [HttpGet, Route("GetAvailable", Name = "Pobierz dostępne indeksy")]
        [SwaggerResponse(System.Net.HttpStatusCode.OK, type: typeof(List<int>))]  
        public HttpResponseMessage GetAvailableIndexes()
        {
            var temp = new List<int>();
            for (int i = 0; i < _matrixes.Count; ++i)
                temp.Add(i);

            return Request.CreateResponse<List<int>>(System.Net.HttpStatusCode.OK, temp);
        }
    }
}
